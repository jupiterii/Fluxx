﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Fluxx.Core;

namespace Fluxx.Reducer
{
    public abstract class TreeReducer<T>
    {
        //functions routes
        private readonly IDictionary<NodeType, Func<Node, T>> _funcs;
        
        protected TreeReducer()
        {
            _funcs = new Dictionary<NodeType, Func<Node, T>>
            {
                //Terminal nodes
                {NodeType.Empty, Empty},
                {NodeType.Number, Number},
                {NodeType.Bool, Bool},
                {NodeType.String, Str},
                //Reference node
                {NodeType.Id, Id},
                {NodeType.Param, Param},
                {NodeType.Extract, Extract},
                //Unary arithmetic operators
                {NodeType.Minus, Minus},
                {NodeType.Plus, Plus},
                //Arithmetic operators
                {NodeType.Add, Add},
                {NodeType.Sub, Sub},
                {NodeType.Div, Div},
                {NodeType.Mul, Mul},
                {NodeType.Mod, Mod},
                {NodeType.Pow, Pow},
                //Boolean operators
                {NodeType.And, And},
                {NodeType.Or, Or},
                {NodeType.Not, Not},
                {NodeType.Equal, Equal},
                {NodeType.Greater, Greater},
                {NodeType.Less, Less},
                //Containers
                {NodeType.Container, Container},
                {NodeType.Parents, Parenthesis},
                //Call operators
                {NodeType.Blank, Blank},
                {NodeType.Arrow, Arrow},
                //Statements
                {NodeType.Cond, Conditional},
                {NodeType.Range, Range},
                {NodeType.Set, SetBuilder},
                //Miscellanous
                {NodeType.Unpack, Unpack},
                {NodeType.Collection, Collection},
            };
        }
        
        public T Reduce(Node n)
        {
            if (_funcs.ContainsKey(n.Type))
                return _funcs[n.Type](n);
			
            throw new Exception($"unreconignized node type : {n.Type}");
        }

        public abstract T Empty(Node n);
        public abstract T Number(Node n);
        public abstract T Bool(Node n);
        public abstract T Str(Node n);
        
        public abstract T Container(Node n);
        public abstract T Parenthesis(Node n);
        public abstract T Id(Node n);
        public abstract T Param(Node n);
        public abstract T Add(Node n);
        public abstract T Sub(Node n);
        public abstract T Div(Node n);
        public abstract T Mul(Node n);
        public abstract T Mod(Node n);
        public abstract T Pow(Node n);
        public abstract T And(Node n);
        public abstract T Or(Node n);
        public abstract T Not(Node n);
        public abstract T Equal(Node n);
        public abstract T Greater(Node n);
        public abstract T Less(Node n);
        public abstract T Minus(Node n);
        public abstract T Plus(Node n);
        public abstract T Blank(Node n);
        public abstract T Arrow(Node n);
        public abstract T Extract(Node n);
        public abstract T Conditional(Node n);
        public abstract T Range(Node n);
        public abstract T SetBuilder(Node n);
        public abstract T Unpack(Node n);
        public abstract T Collection(Node n);
        
    }
}