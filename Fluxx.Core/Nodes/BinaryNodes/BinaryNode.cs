﻿namespace Fluxx.Core
{
    public class BinaryNode : Node
    {
        public Node Left => children[0];
        public Node Right => children[1];
        
    }
}