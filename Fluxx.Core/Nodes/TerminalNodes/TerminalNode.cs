﻿namespace Fluxx.Core
{
    public class TerminalNode<T> : Node
    {
        public T Value { get; set; }
    }
}